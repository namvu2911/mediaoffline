package com.ducnd.mediapro.ui.interf;

import com.ducnd.mediapro.interactor.MusicOfflineInteractor;

/**
 * Created by ducnd on 15/10/2016.
 */

public interface IAllMusicView extends IBaseView {
    void finshLoadItemMusic(MusicOfflineInteractor.LoadItemMusicOfflineEvent event);
}

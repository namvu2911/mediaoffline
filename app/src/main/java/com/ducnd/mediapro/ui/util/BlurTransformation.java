package com.ducnd.mediapro.ui.util;

import android.content.Context;
import android.graphics.Bitmap;

import com.commit451.nativestackblur.NativeStackBlur;
import com.squareup.picasso.Transformation;

/**
 * Created by ducnd on 19/10/2016.
 */

public class BlurTransformation implements Transformation {
    private int radius;

    public BlurTransformation(int radius) {
        this.radius = radius;
//        rs = RenderScript.create(context);
    }

    @Override
    public Bitmap transform(Bitmap bitmap) {
        Bitmap bm = NativeStackBlur.process(bitmap, radius);
        bitmap.recycle();
        return bm;


    }

    @Override
    public String key() {
        return "blur"+radius;
    }
}

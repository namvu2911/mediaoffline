package com.ducnd.mediapro.ui.customview;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ducnd on 13/10/2016.
 */

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    protected IBaseViewHolder interf;
    public BaseViewHolder(View itemView) {
        super(itemView);
        interf = new IBaseViewHolder() {
            @Override
            public int getPosition() {
                return getAdapterPosition();
            }
        };
    }

    public interface IBaseViewHolder{
        int getPosition();
    }

}

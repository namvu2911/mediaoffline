package com.ducnd.mediapro.ui.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ducnd on 13/10/2016.
 */

public class PermissionUtils {
    public static boolean checkStoreage(Activity act, int requestCode) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        List<String> permissions = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(act, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (ActivityCompat.checkSelfPermission(act, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (permissions.size() == 0) {
            return true;
        } else {
            String pers[] = new String[permissions.size()];
            for (int i = 0; i < permissions.size(); i++) {
                pers[i] = permissions.get(i);
            }
            ActivityCompat.requestPermissions(act, pers, requestCode);
            return false;
        }
    }
}

package com.ducnd.mediapro.ui.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.ducnd.mediapro.MediaProApp;
import com.ducnd.mediapro.R;


/**
 * Created by ducnd on 07/09/2016.
 */
public class BackgroundStatusbar extends View {
    public BackgroundStatusbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    public BackgroundStatusbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    private void init(AttributeSet attributeSet, int defStyleAttr) {
        TypedArray array = getContext().obtainStyledAttributes(attributeSet, R.styleable.StausBarStyle, defStyleAttr, 0);
        int colorBg = array.getColor(R.styleable.StausBarStyle_bg, Color.TRANSPARENT);
        setBackgroundColor(colorBg);
        array.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        if ( isInEditMode() ) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT ) {
                setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), 1);
            }else {
                setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec),
                        MediaProApp.getHeightStatusbar() > 0 ?  MediaProApp.getHeightStatusbar() : getResources().getDimensionPixelSize(R.dimen.height_staus_bar));
            }
//        }

    }
}

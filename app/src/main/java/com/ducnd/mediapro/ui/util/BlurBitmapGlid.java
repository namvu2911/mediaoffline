package com.ducnd.mediapro.ui.util;

import android.content.Context;
import android.graphics.Bitmap;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.commit451.nativestackblur.NativeStackBlur;

/**
 * Created by ducnd on 19/10/2016.
 */

public class BlurBitmapGlid implements Transformation<Bitmap> {


    private int mRadius;
    private Context mContext;

    public BlurBitmapGlid(Context context, int radius) {
        mRadius = radius;
        mContext = context;
    }

    @Override
    public Resource<Bitmap> transform(Resource<Bitmap> resource, int outWidth, int outHeight) {
        Bitmap bm = NativeStackBlur.process(resource.get(), mRadius);
        resource.get().recycle();
        return BitmapResource.obtain(bm, Glide.get(mContext).getBitmapPool());
    }

    @Override
    public String getId() {
        return mRadius + "";
    }
}

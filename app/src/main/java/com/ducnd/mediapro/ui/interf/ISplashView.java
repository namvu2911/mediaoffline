package com.ducnd.mediapro.ui.interf;

import com.ducnd.mediapro.interactor.MusicOfflineInteractor;

/**
 * Created by ducnd on 16/10/2016.
 */

public interface ISplashView extends IBaseView {
    void finishCreateServiceAppEvent(MusicOfflineInteractor.FinishCreateServiceAppEvent event);

    void finishLoadFirst(MusicOfflineInteractor.LoadItemMusicOfflineEvent event);
}

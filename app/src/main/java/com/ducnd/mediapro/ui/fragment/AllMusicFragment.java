package com.ducnd.mediapro.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.ducnd.mediapro.Constants;
import com.ducnd.mediapro.R;
import com.ducnd.mediapro.interactor.MusicOfflineInteractor;
import com.ducnd.mediapro.presenter.AllMusicPresenter;
import com.ducnd.mediapro.service.ServiceApp;
import com.ducnd.mediapro.ui.activity.MainActivity;
import com.ducnd.mediapro.ui.activity.PlayMusicActivity;
import com.ducnd.mediapro.ui.adapter.AllMusicAdapter;
import com.ducnd.mediapro.model.ItemMusicOffline;
import com.ducnd.mediapro.ui.interf.IAllMusicView;

/**
 * Created by ducnd on 26/11/2015.
 */
public class AllMusicFragment extends BaseFragment implements AllMusicAdapter.IAllMusicAdapter, SwipeRefreshLayout.OnRefreshListener, IAllMusicView {
    private RecyclerView rvMusic;
    private AllMusicAdapter adapter;
    private ServiceApp mServiceApp;
    private SwipeRefreshLayout refresh;
    private AllMusicPresenter presenter;


    @Override
    public void findViewByIds(View view) {
        rvMusic = (RecyclerView) view.findViewById(R.id.rc_music);
        adapter = new AllMusicAdapter(this);
        refresh = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
    }

    @Override
    public void setEvents(View view) {
        refresh.setOnRefreshListener(this);
    }

    @Override
    public void initComponents(View view) {
        rvMusic.setAdapter(adapter);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getBaseActivity());
        rvMusic.setLayoutManager(manager);
        refresh.setColorSchemeColors(ContextCompat.getColor(getBaseActivity(), R.color.colorPrimary), ContextCompat.getColor(getBaseActivity(), R.color.colorPrimaryDark));
        presenter = new AllMusicPresenter(this);
        presenter.onCreate();
    }

    public void setServiceApp( ServiceApp serviceApp ) {
        mServiceApp =  serviceApp;
    }


    @Override
    public int getLayoutMain() {
        return R.layout.fragment_all_music;
    }

    @Override
    public void onDestroyView() {
        mServiceApp = null;
        presenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int getCount() {
        return MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().size() + 1;
    }

    @Override
    public void onClickItem(int position) {
        ItemMusicOffline itemMusic = MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().get(position);
        mServiceApp.playMusic(itemMusic);
    }

    @Override
    public ItemMusicOffline getItemMusic(int position) {
        return MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().get(position);
    }

    @Override
    public Context getContext() {
        return super.getContext();
    }

    @Override
    public void onRefresh() {
        presenter.getItemMusicOffline(true, true);
    }

    @Override
    public void finshLoadItemMusic(MusicOfflineInteractor.LoadItemMusicOfflineEvent event) {
        adapter.notifyDataSetChanged();
        refresh.setRefreshing(false);
    }


}

package com.ducnd.mediapro.ui.fragment;

import android.net.Uri;
import android.os.Build;
import android.view.View;

import com.ducnd.mediapro.Constants;
import com.ducnd.mediapro.R;
import com.ducnd.mediapro.ui.customview.AnimationCircleImageView;
import com.ducnd.mediapro.ui.util.BlurTransformation;
import com.squareup.picasso.Picasso;

/**
 * Created by ducnd on 20/10/2016.
 */

public class MainLayerMusicFragment extends BaseFragment {
    private AnimationCircleImageView ivThumbnail;

    @Override
    public void findViewByIds(View view) {
        ivThumbnail = (AnimationCircleImageView) view.findViewById(R.id.iv_thumbnail);
    }

    @Override
    public void setEvents(View view) {

    }

    @Override
    public void initComponents(View view) {
        String strUri = getBaseActivity().getIntent().getStringExtra(Constants.EXTRA_URI);
//        Glide.with(this)
//                .load(Uri.parse(strUri))
//                .placeholder(R.drawable.ic_large_launcher)
//                .error(R.drawable.hot_girl)
//                .bitmapTransform(new BlurBitmapGlid(this, 100))
//                .into(ivThumbail);
        Picasso.with(getBaseActivity())
                .load(Uri.parse(strUri))
                .placeholder(R.drawable.ic_large_launcher)
                .error(R.drawable.hot_girl)
                .transform(new BlurTransformation(100))
                .into(ivThumbnail);
    }

    @Override
    public int getLayoutMain() {
        return R.layout.fragment_main_player_music;
    }
}

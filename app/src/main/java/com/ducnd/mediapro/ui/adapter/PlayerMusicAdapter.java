package com.ducnd.mediapro.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.ducnd.mediapro.ui.fragment.ListPlayerMusicFragment;
import com.ducnd.mediapro.ui.fragment.LyricPlayerMusicFragment;
import com.ducnd.mediapro.ui.fragment.MainLayerMusicFragment;

/**
 * Created by ducnd on 20/10/2016.
 */

public class PlayerMusicAdapter extends FragmentPagerAdapter {
    public PlayerMusicAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                return new MainLayerMusicFragment();
            case 2:
                return new LyricPlayerMusicFragment();
            default:
                return new ListPlayerMusicFragment();
        }

    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}

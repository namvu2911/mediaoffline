package com.ducnd.mediapro.ui.interf;

import android.view.View;

import com.ducnd.mediapro.ui.activity.BaseActivity;

/**
 * Created by ducnd on 13/10/2016.
 */

public interface IBaseViewFragment extends IBaseView {
    void findViewByIds(View view);
    void setEvents(View view);
    void initComponents(View view);
    BaseActivity getBaseActivity();
}

package com.ducnd.mediapro.ui.interf;

import com.ducnd.mediapro.interactor.MusicOfflineInteractor;
import com.ducnd.mediapro.model.MusicMessageFromServiceAppEvent;

/**
 * Created by ducnd on 16/10/2016.
 */

public interface IMainView {
    void receiveMusicMessageFromServiceAppEvent(MusicMessageFromServiceAppEvent event);

}

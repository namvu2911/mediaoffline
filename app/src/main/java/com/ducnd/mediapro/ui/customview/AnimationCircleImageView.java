package com.ducnd.mediapro.ui.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;

import com.ducnd.mediapro.R;

/**
 * Created by ducnd on 19/10/2016.
 */

public class AnimationCircleImageView extends CircleImageView {

    public AnimationCircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    public AnimationCircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.AnimationCircleImageView, defStyle, 0);
        int resAnimation = array.getResourceId(R.styleable.AnimationCircleImageView_id_animation, 0);
        if (resAnimation > 0) {
            Animation animation = AnimationUtils.loadAnimation(context, resAnimation);
//            animation.setInterpolator(context, android.R.anim.cycle_interpolator);
            setAnimation(animation);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (getAnimation() != null) {
            getAnimation().start();
        }
    }

    public void cancleAnimation() {
        if ( getAnimation() != null ) {
            getAnimation().cancel();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if (getAnimation() != null) {
            getAnimation().cancel();
        }
        super.onDetachedFromWindow();
    }
}

package com.ducnd.mediapro.ui.interf;

/**
 * Created by ducnd on 13/10/2016.
 */

public interface IBaseView {
    int getLayoutMain();
    void showProgress();
    void hideProgress();
    void showMessage(String message);
    void canCancelProgress(boolean cancel);
}

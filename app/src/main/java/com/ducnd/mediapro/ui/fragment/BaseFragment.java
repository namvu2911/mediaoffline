package com.ducnd.mediapro.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ducnd.mediapro.ui.activity.BaseActivity;
import com.ducnd.mediapro.ui.interf.IBaseViewFragment;

/**
 * Created by ducnd on 13/10/2016.
 */

public abstract class BaseFragment extends Fragment implements IBaseViewFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutMain(), container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewByIds(view);
        initComponents(view);
        setEvents(view);
    }

    @Override
    public BaseActivity getBaseActivity() {
        return ((BaseActivity) getActivity());
    }

    @Override
    public void showProgress() {
        getBaseActivity().showProgress();
    }

    @Override
    public void hideProgress() {
        getBaseActivity().hideProgress();
    }

    @Override
    public void showMessage(String message) {
        getBaseActivity().showMessage(message);
    }

    @Override
    public void canCancelProgress(boolean cancel) {
        getBaseActivity().canCancelProgress(cancel);
    }
}

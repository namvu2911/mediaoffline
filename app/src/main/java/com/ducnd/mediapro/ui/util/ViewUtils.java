package com.ducnd.mediapro.ui.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

/**
 * Created by ducnd on 13/10/2016.
 */

public class ViewUtils {
    public static Fragment getActiveFragment(FragmentManager fragmentManager, ViewPager container, int position) {
        String name = makeFragmentName(container.getId(), position);
        return  fragmentManager.findFragmentByTag(name);
    }

    private static String makeFragmentName(int viewId, int index) {
        return "android:switcher:" + viewId + ":" + index;
    }
}

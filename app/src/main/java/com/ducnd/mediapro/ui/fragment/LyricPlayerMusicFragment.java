package com.ducnd.mediapro.ui.fragment;

import android.view.View;

import com.ducnd.mediapro.R;

/**
 * Created by ducnd on 20/10/2016.
 */

public class LyricPlayerMusicFragment extends BaseFragment {
    @Override
    public void findViewByIds(View view) {

    }

    @Override
    public void setEvents(View view) {

    }

    @Override
    public void initComponents(View view) {

    }

    @Override
    public int getLayoutMain() {
        return R.layout.fragment_player_music_lyric;
    }
}

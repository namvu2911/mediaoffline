package com.ducnd.mediapro.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ducnd.mediapro.R;
import com.ducnd.mediapro.ui.customview.BaseViewHolder;
import com.ducnd.mediapro.model.ItemMusicOffline;

/**
 * Created by ducnd on 13/10/2016.
 */

public class AllMusicAdapter extends RecyclerView.Adapter<AllMusicAdapter.ViewHolder> implements View.OnClickListener {
    public static final int VIEW_TYPE_NULL = 0;
    public static final int VIEW_TYPE_NOMAL = 1;
    private IAllMusicAdapter mInterf;

    public AllMusicAdapter(IAllMusicAdapter interf) {
        mInterf = interf;
    }

    @Override
    public AllMusicAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NULL:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycle_null, parent, false);
                ViewHolder viewHolder = new ViewHolder(view);
                return viewHolder;

            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_music, parent, false);
                viewHolder = new ViewHolder(view, this);
                return viewHolder;
        }


    }

    @Override
    public void onBindViewHolder(AllMusicAdapter.ViewHolder holder, int position) {
        if (holder.getItemViewType() == VIEW_TYPE_NULL) {
            return;
        }
        ItemMusicOffline itemMusic = mInterf.getItemMusic(position);
        holder.tvNameMusic.setText(itemMusic.getTitle());
        holder.tvArtistMusic.setText(itemMusic.getArtist());
        Glide.with(mInterf.getContext())
                .load(itemMusic.getResThumbnail())
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.ivThumbnail);
    }

    @Override
    public int getItemCount() {
        return mInterf.getCount();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.item_music:
                BaseViewHolder.IBaseViewHolder holder = (BaseViewHolder.IBaseViewHolder) v.getTag();
                mInterf.onClickItem(holder.getPosition());
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            return VIEW_TYPE_NULL;
        } else {
            return VIEW_TYPE_NOMAL;
        }
    }

    static class ViewHolder extends BaseViewHolder {
        private CheckBox cBChoosion;
        private ImageView ivThumbnail;
        private ImageView btnMore;
        private TextView tvNameMusic;
        private TextView tvArtistMusic;

        public ViewHolder(View itemView, View.OnClickListener onclick) {
            super(itemView);
            cBChoosion = (CheckBox) itemView.findViewById(R.id.cb_choosion);
            ivThumbnail = (ImageView) itemView.findViewById(R.id.iv_thumbnail);
            btnMore = (ImageView) itemView.findViewById(R.id.btn_more);
            tvNameMusic = (TextView) itemView.findViewById(R.id.tv_name_music);
            tvArtistMusic = (TextView) itemView.findViewById(R.id.tv_artist_music);

            btnMore.setOnClickListener(onclick);
            cBChoosion.setOnClickListener(onclick);
            itemView.setOnClickListener(onclick);

            btnMore.setTag(interf);
            cBChoosion.setTag(interf);
            itemView.setTag(interf);
        }

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface IAllMusicAdapter {
        int getCount();

        ItemMusicOffline getItemMusic(int position);

        Context getContext();

        void onClickItem(int position );
    }
}

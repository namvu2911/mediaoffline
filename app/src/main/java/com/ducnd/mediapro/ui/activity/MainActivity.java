package com.ducnd.mediapro.ui.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.drawable.LevelListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ducnd.mediapro.Constants;
import com.ducnd.mediapro.R;
import com.ducnd.mediapro.interactor.MusicOfflineInteractor;
import com.ducnd.mediapro.model.ItemMusicOffline;
import com.ducnd.mediapro.model.MrgMediaplayer;
import com.ducnd.mediapro.model.MusicMessageFromServiceAppEvent;
import com.ducnd.mediapro.presenter.MainPresenter;
import com.ducnd.mediapro.presenter.interf.IMainPresenter;
import com.ducnd.mediapro.service.ServiceApp;
import com.ducnd.mediapro.ui.adapter.MainFragemtAdapter;
import com.ducnd.mediapro.ui.fragment.AlbumFragment;
import com.ducnd.mediapro.ui.fragment.AllMusicFragment;
import com.ducnd.mediapro.ui.fragment.ArtistFramgent;
import com.ducnd.mediapro.ui.fragment.FolderMusicFrament;
import com.ducnd.mediapro.ui.fragment.PlayListFragment;
import com.ducnd.mediapro.ui.interf.IMainView;
import com.ducnd.mediapro.ui.util.PermissionUtils;
import com.ducnd.mediapro.ui.util.ViewUtils;
import com.squareup.picasso.Picasso;

public class MainActivity extends BaseActivity implements
        TabLayout.OnTabSelectedListener,
        ActivityCompat.OnRequestPermissionsResultCallback, View.OnClickListener,
        IMainView {

    private ViewPager viewpager;
    private MainFragemtAdapter adapter;
    private ImageView ivThubnail;
    private TextView tvMusicName;
    private TextView tvArtistMusic;
    private ImageButton btnPrevious;
    private ImageButton btnNext;
    private ImageButton btnPlay;
    private LevelListDrawable levelImagePlay;
    private ServiceApp mServiceApp;
    private IMainPresenter presenter;


    @Override
    public void findViewByIds() {
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        ivThubnail = (ImageView) findViewById(R.id.iv_thumbnail);
        tvMusicName = (TextView) findViewById(R.id.tv_music_name);
        tvArtistMusic = (TextView) findViewById(R.id.tv_artist_music);
        btnPrevious = (ImageButton) findViewById(R.id.btn_previous);
        btnPlay = (ImageButton) findViewById(R.id.btn_play);
        btnNext = (ImageButton) findViewById(R.id.btn_next);

        levelImagePlay = (LevelListDrawable) btnPlay.getDrawable();
    }

    @Override
    public void setEvents() {
        btnNext.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        findViewById(R.id.rl_bottom).setOnClickListener(this);
        enabaleButtonControlMusic(false);
    }

    private void enabaleButtonControlMusic(boolean enable) {
        btnPrevious.setEnabled(enable);
        btnPlay.setEnabled(enable);
        btnNext.setEnabled(enable);
    }

    @Override
    public void initComponents() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        initToolbar(toolbar);
        initNavigation(toolbar);

        adapter = new MainFragemtAdapter(getSupportFragmentManager());
        try {
            adapter.addFragment(AllMusicFragment.class.newInstance(), getString(R.string.all_music));
            adapter.addFragment(PlayListFragment.class.newInstance(), getString(R.string.play_list));
            adapter.addFragment(AlbumFragment.class.newInstance(), getString(R.string.albums));
            adapter.addFragment(ArtistFramgent.class.newInstance(), getString(R.string.artists));
            adapter.addFragment(FolderMusicFrament.class.newInstance(), getString(R.string.Folders));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        viewpager.setAdapter(adapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewpager);
        tabLayout.addOnTabSelectedListener(this);

        presenter = new MainPresenter(this);
        presenter.onCreate();
        bindService();

    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mServiceApp = ((ServiceApp.BinderServiceApp) service).getServiceApp();
            ((AllMusicFragment)adapter.getItem(0)).setServiceApp(mServiceApp);
            enabaleButtonControlMusic(true);

            if (MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().size() > 0) {
                ItemMusicOffline itemMusic = MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().get(mServiceApp.getCurrentPositionPlay());
                if (itemMusic != null) {
                    updateUIBottom(itemMusic);
                    MrgMediaplayer.State state = mServiceApp.getState();
                    if (state == MrgMediaplayer.State.START) {
                        levelImagePlay.setLevel(1);
                    } else {
                        levelImagePlay.setLevel(0);
                    }
                }

            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private void updateUIBottom(ItemMusicOffline itemMusic) {
//        Glide.with(MainActivity.this)
//                .load(itemMusic.getResThumbnail())
//                .placeholder(R.mipmap.ic_launcher)
//                .error(R.mipmap.ic_launcher)
//                .into(ivThubnail);
        Picasso.with(MainActivity.this)
                .load(itemMusic.getResThumbnail())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(ivThubnail);
        tvMusicName.setText(itemMusic.getTitle());
        tvArtistMusic.setText(itemMusic.getArtist());
    }

    public ServiceApp getServiceApp() {
        return mServiceApp;
    }


    private void bindService() {
        Intent intent = new Intent();
        intent.setClass(this, ServiceApp.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void initToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.app_name);
    }

    private void initNavigation(Toolbar toolbar) {
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.open_navigation_drawer, R.string.close_navigation_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);

    }

    @Override
    public int getLayoutMain() {
        return R.layout.activity_main;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        viewpager.setCurrentItem(position);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    @Override
    protected void onDestroy() {
        if (mServiceApp != null) {
            unbindService(mConnection);
            mServiceApp = null;
        }
        mConnection = null;
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_previous:
                mServiceApp.actionPrevious();
                break;
            case R.id.btn_play:
                mServiceApp.actionPlay();
                break;
            case R.id.btn_next:
                mServiceApp.actionNext();
                break;
            case R.id.rl_bottom:
                ItemMusicOffline itemMusicOffline = MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().get(mServiceApp.getCurrentPositionPlay());
                startPlayMusic(itemMusicOffline.getResThumbnail(), ivThubnail);
                break;
            default:
                break;
        }
    }

    @Override
    public void receiveMusicMessageFromServiceAppEvent(MusicMessageFromServiceAppEvent event) {
        int postion = event.getPostion();
        switch (event.getAction()) {
            case MusicMessageFromServiceAppEvent.ACTION_PREVIOUS:
                updateUIBottom(MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().get(postion));
                break;
            case MusicMessageFromServiceAppEvent.ACTION_PLAY:
                levelImagePlay.setLevel(1);
                break;
            case MusicMessageFromServiceAppEvent.ACTION_PAUSE:
                levelImagePlay.setLevel(0);
                break;
            case MusicMessageFromServiceAppEvent.ACTION_NEXT:
                updateUIBottom(MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().get(postion));
                break;
            case MusicMessageFromServiceAppEvent.ACTION_PREPARE:
                updateUIBottom(MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().get(postion));
                break;
            case MusicMessageFromServiceAppEvent.ACTION_COMPLETE:
                levelImagePlay.setLevel(0);
                break;
            default:
                break;
        }
    }

    private void startPlayMusic(Uri uriThumnail, View view) {
        Intent intent = new Intent(this, PlayMusicActivity.class);
        if (uriThumnail != null) {
            intent.putExtra(Constants.EXTRA_URI, uriThumnail.toString());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, view, "profile");
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }

    }


    //    @Override
//    public void onPreNextOrPrevious(ItemMusicOffline itemMusic) {
//        updateUIBottom(itemMusic);
//    }
//
//    @Override
//    public void onStart(ItemMusicOffline itemMusic) {
//        levelImagePlay.setLevel(1);
//    }
//
//    @Override
//    public void onPause(ItemMusicOffline itemMusic) {
//        levelImagePlay.setLevel(0);
//    }
//
//    @Override
//    public void onComplete(ItemMusicOffline itemMusic) {
//        levelImagePlay.setLevel(0);
//    }


}

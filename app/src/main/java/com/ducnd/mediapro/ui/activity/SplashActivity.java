package com.ducnd.mediapro.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;

import com.ducnd.mediapro.Constants;
import com.ducnd.mediapro.R;
import com.ducnd.mediapro.interactor.MusicOfflineInteractor;
import com.ducnd.mediapro.presenter.SplashPresenter;
import com.ducnd.mediapro.presenter.interf.ISplashPresenter;
import com.ducnd.mediapro.service.ServiceApp;
import com.ducnd.mediapro.ui.interf.ISplashView;
import com.ducnd.mediapro.ui.util.PermissionUtils;

/**
 * Created by ducnd on 14/10/2016.
 */

public class SplashActivity extends BaseActivity implements ISplashView {
    public static final int CHECK_PERMISSION_STORAGE_FOR_ALL_MUSIC_FRAGMENT = 0;
    private ISplashPresenter presenter;
    private long currentTime;

    @Override
    public void findViewByIds() {

    }

    @Override
    public void setEvents() {

    }

    @Override
    public void setViewRoot() {
        getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        super.setViewRoot();
    }

    @Override
    public void initComponents() {
        currentTime = System.currentTimeMillis();
        presenter = new SplashPresenter(this);
        presenter.onCreate();
        if (PermissionUtils.checkStoreage(this, CHECK_PERMISSION_STORAGE_FOR_ALL_MUSIC_FRAGMENT)) {
            checkPermission();
        }

    }

    private void checkPermission() {
        boolean isSuccess = presenter.getItemMusicFirst(true);
        if (isSuccess) {
            startServiceApp();
        }
    }

    @Override
    public int getLayoutMain() {
        return R.layout.splash_activity;
    }

    private void startServiceApp() {
        Intent intent = new Intent();
        intent.setClass(this, ServiceApp.class);
        intent.setAction(Constants.ACTION_START_SERVICE_FROM_SFLASH);
        startService(intent);
    }


    @Override
    public void finishCreateServiceAppEvent(MusicOfflineInteractor.FinishCreateServiceAppEvent event) {
        startMain();
    }

    private void startMain() {
        long current = System.currentTimeMillis();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent inte = new Intent();
                inte.setClass(SplashActivity.this, MainActivity.class);
                finish();
                startActivity(inte);
            }
        }, (3000 - (current - currentTime)) <= 0 ? 0 : (3000 - (current - currentTime)));
    }

    @Override
    public void finishLoadFirst(MusicOfflineInteractor.LoadItemMusicOfflineEvent event) {
        startServiceApp();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                return;
            }
        }
        switch (requestCode) {
            case CHECK_PERMISSION_STORAGE_FOR_ALL_MUSIC_FRAGMENT:
                checkPermission();
                break;
            default:
                break;
        }
    }


    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}

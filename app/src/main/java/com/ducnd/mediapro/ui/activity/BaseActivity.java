package com.ducnd.mediapro.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ducnd.mediapro.R;
import com.ducnd.mediapro.ui.interf.IBaseViewActivity;

/**
 * Created by ducnd on 13/10/2016.
 */

public abstract class BaseActivity extends AppCompatActivity
        implements IBaseViewActivity {
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (hasProgress()) {
            mProgress = new ProgressDialog(this);
            mProgress.setTitle(null);
            mProgress.setMessage(getString(R.string.loading));
        }
        setViewRoot();
        findViewByIds();
        initComponents();
        setEvents();
    }

    @Override
    public void setViewRoot() {
        setContentView(getLayoutMain());
    }

    @Override
    public void hideProgress() {
        if (mProgress != null) {
            mProgress.cancel();
        }
    }



    @Override
    public void showProgress() {
        if (mProgress != null) {
            mProgress.show();
        }
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT);
    }

    @Override
    public void canCancelProgress(boolean cancel) {
        if (mProgress != null) {
            mProgress.setCancelable(cancel);
            mProgress.setCanceledOnTouchOutside(cancel);
        }
    }

    protected boolean hasProgress() {
        return true;
    }
}

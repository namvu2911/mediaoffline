package com.ducnd.mediapro.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ducnd.mediapro.R;

/**
 * Created by ducnd on 26/11/2015.
 */
public class AlbumFragment extends Fragment {
    private static final String TAG = "AlbumFragment";
    private View viewRoot;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if ( container == null ) {
            Log.i(TAG, "onCreateView container null");
        }
        else {
            Log.i(TAG, "onCreateView container not null");
        }
        viewRoot = inflater.inflate(R.layout.fragment_album, container, false);
        return viewRoot;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}

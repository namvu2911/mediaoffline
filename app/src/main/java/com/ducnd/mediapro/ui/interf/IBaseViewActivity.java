package com.ducnd.mediapro.ui.interf;

/**
 * Created by ducnd on 13/10/2016.
 */

public interface IBaseViewActivity extends IBaseView {
    void setViewRoot();
    void findViewByIds();
    void setEvents();
    void initComponents();
}

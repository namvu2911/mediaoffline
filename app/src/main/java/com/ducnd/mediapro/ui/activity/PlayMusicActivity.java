package com.ducnd.mediapro.ui.activity;

import android.net.Uri;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ducnd.mediapro.Constants;
import com.ducnd.mediapro.R;
import com.ducnd.mediapro.ui.adapter.PlayerMusicAdapter;
import com.ducnd.mediapro.ui.customview.AnimationCircleImageView;
import com.ducnd.mediapro.ui.customview.CircleIndicator;
import com.ducnd.mediapro.ui.util.BlurBitmapGlid;
import com.ducnd.mediapro.ui.util.BlurTransformation;
import com.squareup.picasso.Picasso;

/**
 * Created by ducnd on 17/10/2016.
 */

public class PlayMusicActivity extends BaseActivity {
    private ImageView ivBgThumnaill;
    private ViewPager viewPager;


    @Override
    public void findViewByIds() {
        ivBgThumnaill = (ImageView) findViewById(R.id.iv_bg_thumnail);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
    }

    @Override
    public void setEvents() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewPager.setTransitionName("profile");
        }
    }

    @Override
    public void initComponents() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        getSupportActionBar().setTitle(null);

        PlayerMusicAdapter adapter = new PlayerMusicAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(1);
        CircleIndicator indicator = (CircleIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);

        String strUri = getIntent().getStringExtra(Constants.EXTRA_URI);
//        Picasso.with(this)
//                .load(Uri.parse(strUri))
//                .error(R.drawable.bg_play_music)
//                .er
    }

    @Override
    public void setViewRoot() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
//        getWindow().getDecorView().setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LOW_PROFILE |
//                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
//                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
//                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        super.setViewRoot();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public int getLayoutMain() {
        return R.layout.activity_play_music;
    }
}

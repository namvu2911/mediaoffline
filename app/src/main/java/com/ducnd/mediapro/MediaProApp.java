package com.ducnd.mediapro;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.ducnd.mediapro.interactor.MusicOfflineInteractor;

/**
 * Created by ducnd on 13/10/2016.
 */

public class MediaProApp extends MultiDexApplication implements Constants{
    private static Context mConextApp;
    private static int widthScreen, heightScreen, heightStatusbar;
    private static float density;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mConextApp = this;
    }

    public static Context getContextApp() {
        return mConextApp;
    }

    private void initSizeScreen() {
        WindowManager manager = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(metrics);
        widthScreen = metrics.widthPixels;
        heightScreen = metrics.heightPixels;
        density = metrics.density;

        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            heightStatusbar = getResources().getDimensionPixelSize(resourceId);
        }else {
            heightStatusbar = getResources().getDimensionPixelSize(R.dimen.height_staus_bar);
        }
    }

    public static int getWidthScreen() {
        return widthScreen;
    }

    public static int getHeightScreen() {
        return heightScreen;
    }

    public static float getDensity() {
        return density;
    }

    public static int getHeightStatusbar() {
        return heightStatusbar;
    }

}

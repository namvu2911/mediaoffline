package com.ducnd.mediapro.interactor;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;

import com.ducnd.mediapro.Constants;
import com.ducnd.mediapro.MediaProApp;
import com.ducnd.mediapro.model.ItemMusicOffline;
import com.ducnd.mediapro.model.MusicMessageFromServiceAppEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ducnd on 14/10/2016.
 */

public enum MusicOfflineInteractor implements Constants {
    INTERACTOR;

    private final EventBus bus;
    private int currentTotalMusic = 0;

    private List<ItemMusicOffline> mItemMusicOfflines;

    private MusicOfflineInteractor() {
        bus = EventBus.builder().installDefaultEventBus();
        mItemMusicOfflines = new ArrayList<>();
    }

    public void register(final Object o) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            new Handler(MediaProApp.getContextApp().getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    MusicOfflineInteractor.this.bus.register(o);
                }
            });
        } else {
            bus.register(o);
        }
    }

    public void unregister(final Object o) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            new Handler(MediaProApp.getContextApp().getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    MusicOfflineInteractor.this.bus.unregister(o);

                }
            });
        } else {
            bus.unregister(o);
        }
    }

    private void post(Object o) {
        bus.post(o);
    }

    public void sendActionMediaPlayer(int position, String action) {
        post(new MusicMessageFromServiceAppEvent(position, action));
    }

    public void sendActionFinishCreateServiceApp(boolean isSuccess) {
        post(new FinishCreateServiceAppEvent(isSuccess));
    }

    public static class FinishCreateServiceAppEvent {
        private boolean isSuccess;

        public FinishCreateServiceAppEvent(boolean isSuccess) {
            this.isSuccess = isSuccess;
        }

        public boolean isSuccess() {
            return isSuccess;
        }
    }

    public List<ItemMusicOffline> getItemMusicOfflines() {
        return mItemMusicOfflines;
    }

    public boolean loadItemMussicOffline(boolean reload, boolean loadAsync ) {
        if (reload) {
            mItemMusicOfflines.clear();
            currentTotalMusic = 0;
        }

        if (loadAsync) {
            new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    boolean isSuccess = getItemMusicOffline();
                    return isSuccess;
                }

                @Override
                protected void onPostExecute(Boolean success) {
                    post(new LoadItemMusicOfflineEvent(success));
                }
            }.execute();
            return false;
        } else {
            return getItemMusicOffline();
        }

    }

    public boolean loadItemMusicFirst(boolean loadAsync) {
        if (mItemMusicOfflines.size() > 0) {
            return true;
        } else {
            loadItemMussicOffline(true, loadAsync);
            if (loadAsync) {
                return false;
            } else {
                return true;
            }
        }
    }

    public static class LoadItemMusicOfflineEvent {
        private boolean mSuccess;

        public LoadItemMusicOfflineEvent(boolean success) {
            mSuccess = success;
        }

        public boolean isSuccess() {
            return mSuccess;
        }
    }


    private boolean getItemMusicOffline() {

        String[] projection = {
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_ID
        };

//        Cursor c = MediaProApp.getContextApp()
//                .getContentResolver()
//                .query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
//                        projection, null, null,
//                        MediaStore.Audio.Media.DATE_ADDED + " DESC " + "limit " +
//                                Constants.COUNT_OF_PAGE_DEFAULT_LOAD_MUSIC + " offset " + currentTotalMusic);
        Cursor c = MediaProApp.getContextApp()
                .getContentResolver()
                .query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        projection, null, null,
                        MediaStore.Audio.Media.DATE_ADDED + " DESC ");
        if (c == null) {
            return false;
        }

        if (c.getCount() == 0) {
            c.close();
            return false;
        }

        int indexTitle = c.getColumnIndex(projection[0]);
        int indexData = c.getColumnIndex(projection[1]);
        int indexDisplayName = c.getColumnIndex(projection[2]);
        int indexDuration = c.getColumnIndex(projection[3]);
        int indexArtist = c.getColumnIndex(projection[4]);
        int indexAlbume = c.getColumnIndex(projection[5]);
        int indexAlbumId = c.getColumnIndex(projection[6]);
        Uri sArtworkUri = Uri
                .parse("content://media/external/audio/albumart");
        c.moveToFirst();

        while (!c.isAfterLast()) {
            String title = c.getString(indexTitle);
            String data = c.getString(indexData);
            String displayName = c.getString(indexDisplayName);
            int duration = c.getInt(indexDuration);
            String artist = c.getString(indexArtist);
            String albume = c.getString(indexAlbume);
            long idAlbum = c.getLong(indexAlbumId);
            Uri albumArtUri = ContentUris.withAppendedId(sArtworkUri, idAlbum);
            mItemMusicOfflines.add(new ItemMusicOffline(title, displayName, artist, albume, data, duration, albumArtUri));
            c.moveToNext();
        }
        currentTotalMusic = mItemMusicOfflines.size() - 1;
        c.close();
        return true;

    }


}

package com.ducnd.mediapro.presenter.interf;

/**
 * Created by ducnd on 15/10/2016.
 */

public interface IAllMusicPresenter extends IBasePresenter{
    boolean getItemMusicOffline(boolean reload, boolean loadAsync);
}

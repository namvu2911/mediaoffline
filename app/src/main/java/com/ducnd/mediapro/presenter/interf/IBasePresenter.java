package com.ducnd.mediapro.presenter.interf;

/**
 * Created by ducnd on 15/10/2016.
 */

public interface IBasePresenter {
    void onCreate();
    void onDestroy();
}

package com.ducnd.mediapro.presenter;

import com.ducnd.mediapro.interactor.MusicOfflineInteractor;
import com.ducnd.mediapro.model.MusicMessageFromServiceAppEvent;
import com.ducnd.mediapro.presenter.interf.IMainPresenter;
import com.ducnd.mediapro.ui.interf.IMainView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by ducnd on 16/10/2016.
 */

public class MainPresenter implements IMainPresenter {
    private IMainView mView;

    public MainPresenter( IMainView view ) {
        mView = view;
    }
    @Override
    public void onCreate() {
        MusicOfflineInteractor.INTERACTOR.register(this);
    }

    @Override
    public void onDestroy() {
        MusicOfflineInteractor.INTERACTOR.unregister(this);
    }

    @Subscribe
    public void onMusicMessageFromServiceAppEvent(MusicMessageFromServiceAppEvent event) {
        mView.receiveMusicMessageFromServiceAppEvent(event);
    }

}

package com.ducnd.mediapro.presenter;

import com.ducnd.mediapro.interactor.MusicOfflineInteractor;
import com.ducnd.mediapro.presenter.interf.IAllMusicPresenter;
import com.ducnd.mediapro.ui.interf.IAllMusicView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by ducnd on 15/10/2016.
 */

public class AllMusicPresenter implements IAllMusicPresenter {
    private IAllMusicView mView;

    public AllMusicPresenter(IAllMusicView view ) {
        mView = view;
    }

    @Override
    public void onCreate() {
        MusicOfflineInteractor.INTERACTOR.register(this);
    }

    @Override
    public void onDestroy() {
        MusicOfflineInteractor.INTERACTOR.unregister(this);
    }

    @Override
    public boolean getItemMusicOffline(boolean reload, boolean loadAsync) {
        return MusicOfflineInteractor.INTERACTOR.loadItemMussicOffline(reload, loadAsync);
    }



    @Subscribe
    public void onLoadItemMusicOfflineEvent(MusicOfflineInteractor.LoadItemMusicOfflineEvent event ) {
        mView.finshLoadItemMusic(event);
    }

}

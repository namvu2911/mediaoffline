package com.ducnd.mediapro.presenter;

import com.ducnd.mediapro.interactor.MusicOfflineInteractor;
import com.ducnd.mediapro.presenter.interf.ISplashPresenter;
import com.ducnd.mediapro.ui.interf.ISplashView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by ducnd on 16/10/2016.
 */

public class SplashPresenter implements ISplashPresenter {
    private ISplashView mView;

    public SplashPresenter(ISplashView view) {
        mView = view;
    }

    @Override
    public void onCreate() {
        MusicOfflineInteractor.INTERACTOR.register(this);
    }

    @Override
    public void onDestroy() {
        MusicOfflineInteractor.INTERACTOR.unregister(this);

    }

    @Override
    public boolean getItemMusicFirst(boolean loadAsync) {
        boolean isSuccess = MusicOfflineInteractor.INTERACTOR.loadItemMusicFirst(loadAsync);
        return isSuccess;
    }


    @Subscribe
    public void onActionFinishCreateServiceApp(MusicOfflineInteractor.FinishCreateServiceAppEvent event) {
        mView.finishCreateServiceAppEvent(event);
    }

    @Subscribe
    public void onLoadItemMusicOfflineEvent(MusicOfflineInteractor.LoadItemMusicOfflineEvent event) {
        mView.finishLoadFirst(event);
    }
}

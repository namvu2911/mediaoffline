package com.ducnd.mediapro;

/**
 * Created by ducnd on 13/10/2016.
 */

public interface Constants {
    int MAX_QUERY_MUSIC = 30;
    String ACTION_START_SERVICE_FROM_SFLASH = "ACTION_START_SERVICE_FROM_SFLASH";
    String ACTION_FINISH_START_SERVICE_APP = "ACTION_FINISH_START_SERVICE_APP";
    int COUNT_OF_PAGE_DEFAULT_LOAD_MUSIC = 30;
    String EXTRA_URI = "EXTRA_URI";
}

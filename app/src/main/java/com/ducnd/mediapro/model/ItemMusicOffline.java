package com.ducnd.mediapro.model;

import android.net.Uri;

/**
 * Created by ducnd on 26/11/2015.
 */
public class ItemMusicOffline {
    private String mTitle;
    private String mNameMusic;
    private String mArtist;
    private String mAlbume;
    private String mPath;
    private int mDuration;
    private Uri mResThumbnail;

    public ItemMusicOffline(String title, String nameMusic, String artist, String albume, String path, int duration, Uri resThumbnail) {
        mTitle = title;
        mNameMusic = nameMusic;
        mArtist = artist;
        mAlbume = albume;
        mPath = path;
        mDuration = duration;
        mResThumbnail = resThumbnail;
    }

    public String getNameMusic() {
        return mNameMusic;
    }

    public void setNameMusic(String nameMusic) {
        mNameMusic = nameMusic;
    }

    public String getArtist() {
        return mArtist;
    }

    public void setArtist(String artist) {
        mArtist = artist;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getAlbume() {
        return mAlbume;
    }

    public void setAlbume(String albume) {
        mAlbume = albume;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int duration) {
        mDuration = duration;
    }

    public Uri getResThumbnail() {
        return mResThumbnail;
    }
}

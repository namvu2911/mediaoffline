package com.ducnd.mediapro.model;

/**
 * Created by ducnd on 16/10/2016.
 */

public class MusicMessageFromServiceAppEvent {
    public static final String ACTION_PLAY = "ACTION_PLAY";
    public static final String ACTION_PAUSE = "ACTION_PAUSE";
    public static final String ACTION_PREVIOUS = "ACTION_PREVIOUS";
    public static final String ACTION_NEXT = "ACTION_NEXT";
    public static final String ACTION_COMPLETE = "ACTION_COMPLETE";
    public static final String ACTION_PREPARE = "ACTION_PREPARE";
    private int mPosition;
    private String mAction;

    public MusicMessageFromServiceAppEvent(int position, String action) {
        mPosition = position;
        mAction = action;
    }

    public int getPostion() {
        return mPosition;
    }

    public String getAction() {
        return mAction;
    }
}

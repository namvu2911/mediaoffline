package com.ducnd.mediapro.model;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;

/**
 * Created by ducnd on 14/10/2016.
 */

public class MrgMediaplayer implements MediaPlayer.OnPreparedListener, MediaPlayer.OnBufferingUpdateListener,
        MediaPlayer.OnCompletionListener {
    private static final String TAG = MrgMediaplayer.class.getSimpleName();
    private MediaPlayer mPlayer;
    private State state;
    private OnAudioPlayerMrg mInterf;
    private int mPosistion;

    public MrgMediaplayer(OnAudioPlayerMrg interf) {
        mInterf = interf;
        state = State.IDLE;
    }

    public void setDataSource(String dataSource, int position)
            throws IOException {
        mPlayer.setDataSource(dataSource);
        mPosistion = position;
        state = State.INIT;
    }

    public void prepareAsyn() {
        mPlayer.setOnBufferingUpdateListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnPreparedListener(this);
        state = State.PREPARING;
        mPlayer.prepareAsync();
    }


    public int currentPosion() {
        return mPlayer.getCurrentPosition();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        state = State.PREPARED;
        start();
    }

    public void start() {
        if (state == State.PREPARED ||
                state == State.STOP ||
                state == State.PAUSE) {
            mPlayer.start();
            mInterf.onStart(mPosistion);
            state = State.START;
        } else {
            if (state == State.INIT) {
                prepareAsyn();
            }
        }
    }

    public void pause() {
        if (state == State.START) {
            mPlayer.pause();
            state = State.PAUSE;
            mInterf.onPause(mPosistion);
        }
    }

    public void stop() {
        if (state == State.START || state == State.PAUSE) {
            mPlayer.stop();
            state = State.STOP;
        }

    }

    public void seek(int positioin) {
        if (positioin < mPlayer.getDuration() ||
                state == State.PAUSE || state == State.START ||
                state == State.PREPARED) {
            mPlayer.seekTo(positioin);
        }

    }

    public void loop(boolean loop) {
        if (state != State.IDLE) {
            mPlayer.setLooping(loop);
        }
    }

    public void release() {
        mPlayer.release();
        mPlayer = null;
    }

    public void createMediaplayer() {
        if (mPlayer != null) {
            mPlayer.release();
        }
        state = State.IDLE;
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    public State getState() {
        return state;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        Log.d(TAG, "onBufferingUpdate....percent: " + percent);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mInterf.onComplete(mPosistion);
    }

    public void actionPlay() {
        if (state == State.PAUSE || state == State.STOP) {
            start();
            return;
        }

        if (state == State.START) {
            pause();
            return;
        }
        if (state == State.INIT) {
            prepareAsyn();
        }
    }

    public static enum State {
        PREPARING(0),
        START(1),
        STOP(2),
        PAUSE(3),
        RELEASE(4),
        IDLE(5),
        INIT(6),
        PREPARED(7);

        private int value;

        private State(int value) {
            this.value = value;
        }

        public void gigi() {

        }
    }

    public interface OnAudioPlayerMrg {
        void onStart(int position);

        void onPause(int position);

        void onSeek(int position, int toSeek);

        void onComplete(int position);
    }

}

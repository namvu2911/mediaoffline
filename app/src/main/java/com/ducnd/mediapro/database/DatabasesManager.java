package com.ducnd.mediapro.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.widget.Toast;

import com.ducnd.mediapro.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ducnd on 29/11/2015.
 */
public class DatabasesManager implements Constants{
    private static final String TAG = DatabasesManager.class.getSimpleName();
    private Context mContext;
    private static final String DATA_PATH = Environment.getDataDirectory()
            + "/data/com.ducnd.mediaplayer/databases/";
    private SQLiteDatabase sqLiteDatabase;
    private static final String DATA_NAME = "infor_musics";

    public DatabasesManager(Context context) {
        this.mContext = context;
        coppyDataBase();
    }

    private void coppyDataBase() {
        new File(DATA_PATH).mkdir();
        File fileWriteDataBase = new File(DATA_PATH + DATA_NAME);
        if (fileWriteDataBase.exists()) return;
        try {
            fileWriteDataBase.createNewFile();
            AssetManager asset = this.mContext.getAssets();
            InputStream fileRead = asset.open(DATA_NAME);
            FileOutputStream fileWrite = new FileOutputStream(fileWriteDataBase);

            int len = -1;
            byte[] b = new byte[1024];
            while ((len = fileRead.read(b)) > 0) {
                fileWrite.write(b, 0, len);
            }

            fileRead.close();
            fileWrite.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openDataBases() {
        if (sqLiteDatabase == null || sqLiteDatabase.isOpen() == false)
            sqLiteDatabase = SQLiteDatabase.openDatabase(DATA_PATH + DATA_NAME, null,
                    SQLiteDatabase.OPEN_READWRITE);
    }

    public void insertData(String tableName, ContentValues values) {
        openDataBases();
        long index = sqLiteDatabase.insert(tableName, null, values);
        if (index == -1)
            Toast.makeText(mContext, "Insert data failure!", Toast.LENGTH_SHORT)
                    .show();
        else
            Toast.makeText(mContext, "Insert data successfully!",
                    Toast.LENGTH_SHORT).show();
    }

    public void deleteData(String tableName, String whereClause,
                           String[] whereArgs) {
        openDataBases();
        long index = sqLiteDatabase.delete(tableName, whereClause, whereArgs);
        if (index == 0)
            Toast.makeText(mContext, "Nothing is deleted!", Toast.LENGTH_SHORT)
                    .show();
        else
            Toast.makeText(mContext, index + " rows are deleted!",
                    Toast.LENGTH_SHORT).show();
    }

    public void updateData(String tableName, ContentValues values,
                           String whereClause, String[] whereArgs) {
        openDataBases();

        long index = sqLiteDatabase.update(tableName, values, whereClause, whereArgs);
        if (index == 0)
            Toast.makeText(mContext, "Nothing is updated!", Toast.LENGTH_SHORT)
                    .show();
        else
            Toast.makeText(mContext, index + " rows are updated!",
                    Toast.LENGTH_SHORT).show();
    }

    public void deleteAllData( String table) {
        this.openDataBases();
        sqLiteDatabase.execSQL("DELETE FROM " + table);
    }

    public void closeDataBase() {
        if (sqLiteDatabase != null && sqLiteDatabase.isOpen())
            sqLiteDatabase.close();
    }


}

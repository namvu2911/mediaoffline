package com.ducnd.mediapro.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Binder;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.RemoteViews;

import com.ducnd.mediapro.Constants;
import com.ducnd.mediapro.R;
import com.ducnd.mediapro.interactor.MusicOfflineInteractor;
import com.ducnd.mediapro.model.ItemMusicOffline;
import com.ducnd.mediapro.model.MrgMediaplayer;
import com.ducnd.mediapro.model.MusicMessageFromServiceAppEvent;
import com.ducnd.mediapro.ui.activity.MainActivity;

import java.io.IOException;
import java.util.List;

/**
 * Created by ducnd on 13/10/2016.
 */

public class ServiceApp extends Service implements MrgMediaplayer.OnAudioPlayerMrg {
    private static final String TAG = "ServiceApp";
    private static final String ACTION_PLAY = "ACTION_PLAY";
    private static final String ACTION_PREVIOUS = "ACTION_PREVIOUS";
    private static final String ACTION_NEXT = "ACTION_NEXT";
    private static final String ACTION_CANCEL = "ACTION_CANCEL";
    private static final String ACTION_FROM_SERVICE_MEDIAPLAYER = "ACTION_FROM_SERVICE_MEDIAPLAYER";

    private static final int ID_NOTIFICATION_MUSIC = 101;


    private int currentPositionPlay = -1;
    private MrgMediaplayer mrgMediaplayer;

    private PendingIntent pendingIntentPlay;
    private PendingIntent pendingIntentPrevious;
    private PendingIntent pendingIntentNext;
    private PendingIntent pendingIntentCancel;
    private PendingIntent pendingIntentContent;
    private NotificationManagerCompat managerCompat;

    private BroadcastServiceMediaplayer mBroadcastServiceMediaplayer;

    @Override
    public void onCreate() {
        super.onCreate();
        mrgMediaplayer = new MrgMediaplayer(this);

        initBroastCastService();
        initPendingIntent();
        //notification manager
        managerCompat = NotificationManagerCompat.from(this);


        if (MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().size() > 0) {
            currentPositionPlay = 0;
            initMediaPlay(MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().get(0), currentPositionPlay);
        }
    }

    private void initMediaPlay(ItemMusicOffline itemMusic, int position) {
        try {
            mrgMediaplayer.createMediaplayer();
            mrgMediaplayer.setDataSource(itemMusic.getPath(), position);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initPendingIntent() {
        Intent intentPlay = new Intent();
        intentPlay.setAction(ACTION_PLAY);
        pendingIntentPlay = PendingIntent.getBroadcast(this, 0, intentPlay, PendingIntent.FLAG_UPDATE_CURRENT);


        Intent intentPrevious = new Intent();
        intentPrevious.setAction(ACTION_PREVIOUS);
        pendingIntentPrevious = PendingIntent.getBroadcast(this, 1, intentPrevious, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentNext = new Intent();
        intentNext.setAction(ACTION_NEXT);
        pendingIntentNext = PendingIntent.getBroadcast(this, 2, intentNext, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentCancel = new Intent();
        intentCancel.setAction(ACTION_CANCEL);
        pendingIntentCancel = PendingIntent.getBroadcast(this, 2, intentCancel, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentContent = new Intent();
        intentContent.setAction(ACTION_FROM_SERVICE_MEDIAPLAYER);
        intentContent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intentContent.setClass(this, MainActivity.class);

        pendingIntentContent = PendingIntent.getActivity(this, 2, intentContent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void initBroastCastService() {
        mBroadcastServiceMediaplayer = new BroadcastServiceMediaplayer();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_CANCEL);
        filter.addAction(ACTION_NEXT);
        filter.addAction(ACTION_PLAY);
        filter.addAction(ACTION_PREVIOUS);
        registerReceiver(mBroadcastServiceMediaplayer, filter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (Constants.ACTION_START_SERVICE_FROM_SFLASH.equals(intent.getAction())) {
                MusicOfflineInteractor.INTERACTOR.sendActionFinishCreateServiceApp(true);
            }
        }
        return START_NOT_STICKY;
    }

    public int getCurrentPositionPlay() {
        return currentPositionPlay;
    }

    public MrgMediaplayer.State getState() {
        return mrgMediaplayer.getState();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new BinderServiceApp();
    }



    public void playMusic(ItemMusicOffline itemMusic) {
        int index = MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().indexOf(itemMusic);
        if (index > -1) {
            currentPositionPlay = index;
            mrgMediaplayer.createMediaplayer();
            MusicOfflineInteractor.INTERACTOR.sendActionMediaPlayer(currentPositionPlay, MusicMessageFromServiceAppEvent.ACTION_PREPARE);
            try {
                mrgMediaplayer.setDataSource(itemMusic.getPath(), index);
                mrgMediaplayer.prepareAsyn();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public class BinderServiceApp extends Binder {
        public ServiceApp getServiceApp() {
            return ServiceApp.this;
        }
    }


    private void createNotification(ItemMusicOffline itemMusic, boolean isPlay) {
        NotificationCompat.Builder builderNotifaction = new NotificationCompat.Builder(this);

        //tao remoteview tu xml
        RemoteViews views = new RemoteViews(getPackageName(), R.layout.layout_small_notification);
        RemoteViews bigContentView = new RemoteViews(getPackageName(), R.layout.layout_notification_player);


        bigContentView.setTextColor(R.id.tv_name, Color.DKGRAY);
        bigContentView.setTextColor(R.id.tv_artist, Color.DKGRAY);
        bigContentView.setTextColor(R.id.tv_title, Color.parseColor("#616161"));

        views.setTextColor(R.id.tv_name, Color.DKGRAY);
        views.setTextColor(R.id.tv_artist, Color.DKGRAY);

        //
        Bitmap art = null;
        if (itemMusic.getResThumbnail() == null) {
            art = BitmapFactory.decodeResource(getResources(), R.drawable.hot_girl);
        } else {
            try {
                art = MediaStore.Images.Media.getBitmap(this.getContentResolver(), itemMusic.getResThumbnail());
                if (art == null) {
                    art = BitmapFactory.decodeResource(getResources(), R.drawable.hot_girl);
                }
            } catch (IOException e) {
                art = BitmapFactory.decodeResource(getResources(), R.drawable.hot_girl);
            }
        }
        views.setImageViewBitmap(R.id.iv_thumbnail, art);
        views.setTextViewText(R.id.tv_name, itemMusic.getTitle());

        bigContentView.setImageViewBitmap(R.id.iv_thumbnail, art);
        bigContentView.setTextViewText(R.id.tv_name, itemMusic.getTitle());
        if (itemMusic.getArtist() == null) {
            bigContentView.setTextViewText(R.id.tv_artist, "Unknown...");
            views.setTextViewText(R.id.tv_artist, "Unknown...");
        } else {
            bigContentView.setTextViewText(R.id.tv_artist, itemMusic.getArtist());
            views.setTextViewText(R.id.tv_artist, itemMusic.getArtist());
        }

        views.setOnClickPendingIntent(R.id.btn_play, pendingIntentPlay);
        views.setOnClickPendingIntent(R.id.btn_previous, pendingIntentPrevious);
        views.setOnClickPendingIntent(R.id.btn_next, pendingIntentNext);

        bigContentView.setOnClickPendingIntent(R.id.btn_play, pendingIntentPlay);
        bigContentView.setOnClickPendingIntent(R.id.btn_previous, pendingIntentPrevious);
        bigContentView.setOnClickPendingIntent(R.id.btn_next, pendingIntentNext);


        if (isPlay) {
            views.setImageViewResource(R.id.btn_play, android.R.drawable.ic_media_pause);


            bigContentView.setImageViewResource(R.id.btn_play, android.R.drawable.ic_media_pause);
        } else {
            views.setImageViewResource(R.id.btn_play, android.R.drawable.ic_media_play);

            bigContentView.setImageViewResource(R.id.btn_play, android.R.drawable.ic_media_play);
        }

        builderNotifaction.setCustomContentView(views);
        builderNotifaction.setCustomBigContentView(bigContentView);
        builderNotifaction.setSmallIcon(R.drawable.ic_music_48dp);
        builderNotifaction.setWhen(System.currentTimeMillis());

        builderNotifaction.setContentIntent(pendingIntentContent);
//        stopForeground(false);
        if (isPlay) {
            startForeground(ID_NOTIFICATION_MUSIC, builderNotifaction.build());
        } else {
            stopForeground(false);
            managerCompat.notify(ID_NOTIFICATION_MUSIC, builderNotifaction.build());
        }


    }

    public void actionPlay() {
        mrgMediaplayer.actionPlay();
    }


    public void actionPrevious() {
        if (MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().size() == 0) {
            return;
        }
        if (currentPositionPlay == 0) {
            currentPositionPlay = MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().size() - 1;
        } else {
            currentPositionPlay = currentPositionPlay - 1;
        }
        mrgMediaplayer.createMediaplayer();
        try {
            ItemMusicOffline itemMusic = MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines().get(currentPositionPlay);
            MusicOfflineInteractor.INTERACTOR.sendActionMediaPlayer(currentPositionPlay, MusicMessageFromServiceAppEvent.ACTION_PREVIOUS);
            mrgMediaplayer.setDataSource(itemMusic.getPath(), currentPositionPlay);
            mrgMediaplayer.prepareAsyn();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void actionNext() {
        List<ItemMusicOffline> itemMusicOfflines = MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines();
        if (itemMusicOfflines.size() == 0) {
            return;
        }
        currentPositionPlay = (currentPositionPlay + 1) % itemMusicOfflines.size();
        mrgMediaplayer.createMediaplayer();
        try {
            ItemMusicOffline itemMusic = itemMusicOfflines.get(currentPositionPlay);
            MusicOfflineInteractor.INTERACTOR.sendActionMediaPlayer(currentPositionPlay, ACTION_NEXT);
            mrgMediaplayer.setDataSource(itemMusic.getPath(), currentPositionPlay);
            mrgMediaplayer.prepareAsyn();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onStart(int position) {
        List<ItemMusicOffline> itemMusicOfflines = MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines();
        MusicOfflineInteractor.INTERACTOR.sendActionMediaPlayer(position, MusicMessageFromServiceAppEvent.ACTION_PLAY);
        createNotification(itemMusicOfflines.get(position), true);
    }

    @Override
    public void onPause(int position) {
        List<ItemMusicOffline> itemMusicOfflines = MusicOfflineInteractor.INTERACTOR.getItemMusicOfflines();
        MusicOfflineInteractor.INTERACTOR.sendActionMediaPlayer(position, MusicMessageFromServiceAppEvent.ACTION_PAUSE);
        createNotification(itemMusicOfflines.get(position), false);
    }

    @Override
    public void onComplete(int position) {
        MusicOfflineInteractor.INTERACTOR.sendActionMediaPlayer(position, MusicMessageFromServiceAppEvent.ACTION_COMPLETE);
        actionNext();
    }

    @Override
    public void onSeek(int position, int toSeek) {

    }

    public class BroadcastServiceMediaplayer extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getAction()) {
                case ACTION_PLAY:
                    if (mrgMediaplayer.getState() == MrgMediaplayer.State.START) {
                        mrgMediaplayer.pause();
                    } else {
                        mrgMediaplayer.start();
                    }
                    break;
                case ACTION_PREVIOUS:
                    actionPrevious();
                    break;
                case ACTION_NEXT:
                    actionNext();
                    break;
                case ACTION_CANCEL:
                    stopForeground(false);
                    managerCompat.cancel(ID_NOTIFICATION_MUSIC);
                    break;
                default:
                    break;
            }

        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mBroadcastServiceMediaplayer);
        mBroadcastServiceMediaplayer = null;
        super.onDestroy();
    }


}
